package com.ssg.intelligentstove;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "ACTIVITY_BLUETOOTH";
    public static final String NAME = "Nombre";
    public static final String MAC = "Mac";
    public static final int REQUEST_ENABLE_BT = 1;

    BluetoothAdapter bluetoothAdapter;
    private ArrayList<BluetoothDevice> listBluetoothDevice = new ArrayList<>();
    LinkedList<HashMap<String, String>> linkedListPairedDevices = new LinkedList<>();
    LinkedList<HashMap<String,String>> linkedListDiscoveredDevices = new LinkedList<>();

    TextView textViewDiscoveredDevices;
    ListView listViewPairedDevices;
    ListView listViewDiscoveredDevices;
    SpecialAdapter adapterPairedDevices;
    SpecialAdapter adapterDiscoveredDevices;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listViewPairedDevices =     (ListView) findViewById(R.id.listViewPairedDevices);
        listViewDiscoveredDevices = (ListView) findViewById(R.id.listViewDiscoveredDevices);
        textViewDiscoveredDevices = (TextView) findViewById(R.id.textViewDiscoveredDevices);

        searchBluetooth();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver,filter);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Buscado dispositivos, espere...");
        dialog.setCancelable(false);

        listViewDiscoveredDevices.setOnItemClickListener(clickDiscoveredDevice);
        listViewPairedDevices.setOnItemClickListener(clickPairedDevice);
    }
}
