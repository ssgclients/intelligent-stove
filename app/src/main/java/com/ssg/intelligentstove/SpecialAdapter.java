package com.ssg.intelligentstove;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by jorge on 4/03/16.
 */
public class SpecialAdapter extends SimpleAdapter{
    public SpecialAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =  super.getView(position, convertView, parent);

        TextView name = (TextView) view.findViewById(android.R.id.text1);
        TextView mac = (TextView) view.findViewById(android.R.id.text2);

        name.setTextColor(Color.parseColor("#212121"));
        mac.setTextColor(Color.parseColor("#727272"));

        return view;
    }
}
